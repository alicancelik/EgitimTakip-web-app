import React from 'react'
import { Link } from 'react-router'

class Header extends React.Component {
  constructor () {              // Bellekte bu sınıf oluşturuldugunda ilk bu kod bölümü çalışır. New Header dersen ilk bu gelir.
    super()                     // Super dediğimizde ise Headerın extends edilmiş sınıfının consturcterı çağırılıyor.
    this.state = {              // Bellek te ki Header ı kullanmak için
      isNavOpen : false
    }
  }
  buttonClicked () {
    this.setState({
      isNavOpen : !this.state.isNavOpen
    })
  }
  render () {
    console.log('RENDERING')
    return (
      <header>
        <nav className=' container navbar navbar-expand-lg navbar-light justify-content-between'>
          <a className='navbar-brand' href='#'>DersTakip</a>
          <button className='navbar-toggler' type='button' onClick={this.buttonClicked.bind(this)}>
            <span className='navbar-toggler-icon' />
          </button>
          <div className='navbar-collapse' style={{ display: this.state.isNavOpen ? 'block' : 'none' }}>
            <ul className='navbar-nav'>
              <li className='nav-item'>
                <Link to='/' activeClassName='active' className='nav-link'>Ana Sayfa</Link>
              </li>
              <li className='nav-item'>
                <Link to='/hakkımızda' activeClassName='active' className='nav-link'>Hakkımızda</Link>
              </li>
              <li className='nav-item'>
                <Link to='/iletisim' activeClassName='active' className='nav-link'>Iletisim</Link>
              </li>
            </ul>
          </div>
        </nav>
      </header>

    )
  }
}
export default Header
